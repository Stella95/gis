<?php

/**
 * Implements hook_block_info().
 */
function olebs_block_info() {
  $maps = _olebs_get_maps_with_blockswitcher();
  $blocks = array();

  foreach ($maps as $key => $map) {
    $blockid = _olebs_get_block_id($key);
    $blocks[$blockid]['info'] = t('OpenLayers Block switcher for ' . $map->title);
  }

  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function olebs_block_view($delta = '') {
  $block = array();
  $maps = _olebs_get_maps_with_blockswitcher();
  $map_name = _olebs_get_map_name($delta);

  if ($map_name != FALSE) {
    $map = $maps[$map_name];

    $olebs_config = _olebs_get_default_config();
    if (isset($map->data['behaviors']['olebs_behavior_blockswitcher'])) {
      $olebs_config = $map->data['behaviors']['olebs_behavior_blockswitcher'];
    }

    $config = array(
      'map_name' => $map_name,
    ) + $olebs_config;

    $form = drupal_get_form('olebs_blockswitcher_form', $config);

    $block['subject'] = t('Map layers');
    $block['content'] = drupal_render($form);
  }

  return $block;
}

/**
 * Implements hook_openlayers_behaviors().
 *
 * This is a ctools plugins hook.
 */
function olebs_openlayers_behaviors() {
  return array(
    'olebs_behavior_blockswitcher' => array(
      'title' => t('OpenLayers External Block Switcher'),
      'type' => 'ui',
      'description' => t('When enabled, a new block is available and displays checkboxes and radios to enable or disable layers.'),
      'behavior' => array(
        'path' => drupal_get_path('module', 'olebs') .'/behaviors',
        'file' => 'olebs_behavior_blockswitcher.inc',
        'class' => 'olebs_behavior_blockswitcher',
        'parent' => 'openlayers_behavior',
      ),
    ),
  );
}

/**
 * Form for the layer switcher.
 *
 * @param $form
 * @param $form_state
 * @param $map
 * @return mixed
 */
function olebs_blockswitcher_form($form, &$form_state, $config) {
  $options_baselayers = array();
  $options_overlays = array();
  $options_overlays_selected = array();
  $config += _olebs_get_default_config();

  $maps = _olebs_get_maps_with_blockswitcher();
  $map = $maps[$config['map_name']];

  foreach($map->data['layers'] as $key => $title) {
    $layer = openlayers_layer_load($key);
    if (isset($layer->data['isBaseLayer']) && $layer->data['isBaseLayer'] == TRUE) {
      $options_baselayers[$key] = $layer->title;
    } else {
      $options_overlays[$key] = $layer->title;
    }
  }

  foreach($map->data['layer_activated'] as $key => $activated) {
    if ($activated === $key) {
      $options_overlays_selected[$key] = $key;
    }
  }

  foreach($map->data['layer_switcher'] as $key => $activated) {
    if ($activated !== $key) {
      unset($options_overlays_selected[$key]);
      unset($options_overlays[$key]);
    }
  }

  if (count($options_baselayers) > 1) {
    $form['baselayers']= array(
      '#title'   => t('Base layers'),
      '#type'    => $config['baselayers_element_type'],
      '#options' => $options_baselayers,
      '#default_value' => $map->data['default_layer'],
    );
  }

  if (!empty($options_overlays)) {
    $form['overlays']= array(
      '#title'   => t('Overlays'),
      '#type'    => 'checkboxes',
      '#options' => $options_overlays,
      '#default_value' => $options_overlays_selected,
    );
  }

  if ($options_baselayers || $options_overlays) {
    $form['map']= array(
      '#type'    => 'hidden',
      '#value' => $map->name,
    );
    $form['#attached']['css'] = array();
    $form['#attached']['library'] = array();
    $form['#attached']['js'][] = array(
      'data' => drupal_get_path('module', 'olebs') . '/behaviors/olebs_behavior_blockswitcher.js',
      'type' => 'file'
    );

  }

  return $form;
}

/**
 * Helper function.
 * Returns maps with the blockswitcher behavior enabled.
 *
 * @return array
 */
function _olebs_get_maps_with_blockswitcher() {
  $maps = &drupal_static(__FUNCTION__);

  if (!isset($maps)) {
    $maps = openlayers_maps();
    foreach ($maps as $key => $map) {
      if (!isset($map->data['behaviors']['olebs_behavior_blockswitcher'])) {
        unset($maps[$key]);
      }
    }
  }

  return $maps;
}

/**
 * Helper function.
 * Returns default configuration.
 *
 * @return array
 */
function _olebs_get_default_config() {
  return array(
    'baselayers_element_type' => 'radios'
  );
}

/**
 * Helper function
 * Return the blocks delta according to the map_name.
 */
function _olebs_get_block_id($map_name) {
  return substr(sha1($map_name), 0, -32) . '_blockswitcher';
}

/**
 * Helper function
 * Return the map name according to the block delta.
 */
function _olebs_get_map_name($delta) {
  foreach (_olebs_get_maps_with_blockswitcher() as $map_name => $map) {
    if (_olebs_get_block_id($map_name) == $delta) {
      return $map_name;
    }
  }
  return FALSE;
}
